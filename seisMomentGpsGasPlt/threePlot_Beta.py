#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function
import sys
sys.path.insert(1, '/home/sindrim/anaconda3/Verkefni/multiplots/latlonPlt/')
sys.path.insert(1, '/home/sindrim/anaconda3/Verkefni/multiplots/GetComponents/')
import plotComponents as plc

# num1 = starting time, num2 = ending time.
# timeRange1 = start time for the second range, timeRange1 = ending time for second range
def main(num1, num2, timeRange1, timeRange2,  minlat, maxlat, minlon, maxlon, sid):
    import cparser
    import logging
    from pathlib import Path
    from datetime import datetime as dt
    import gtimes.timefunc as tf
    import timesmatplt.timesmatplt as tplt  # plot GPS data?
    import functions as ft
    
    dstr="%Y-%m-%d %H:%M:%S"
    dfstr="%Y-%m-%d"  # date format
    
    ### INPUT:
    start = tf.currDatetime(num1)
    start_date = dt.strftime(start, dstr)
    end_date =  plc.getEndDate(num2)
    
    #Create second time period for the seimic activity
    start2 = tf.currDatetime(timeRange1)
    start_date2 = dt.strftime(start2, dstr)
    end_date2 = plc.getEndDate(timeRange2)
        
    StaPars = cparser.Parser()
    
    dt_start = tf.toDatetime(start_date,dstr) # datetime obj
    dt_end = tf.toDatetime(end_date,dstr)
    f_start = tf.currYearfDate(refday=dt_start) # float (fractional year)
    f_end = tf.currYearfDate(refday=dt_end)

    dt_start2 = tf.toDatetime(start_date2,dstr) # datetime obj
    dt_end2 = tf.toDatetime(end_date2,dstr)
    f_start2 = tf.currYearfDate(refday=dt_start2) # float (fractional year)
    f_end2 = tf.currYearfDate(refday=dt_end2)
    
    constr = plc.getConstr(sid)
    
    #load the data from database
    GPS = plc.getGPS(sid, dt_end, dt_start)
    Seis =  plc.getDataBase(dt_start, dt_end, sid)
    Seis2 =  plc.getDataBase(dt_start2, dt_end2, sid)
    
    ## create the figure
    fig = ft.spltbbthreeFrame(Ylabel=None, Title=None)
    fig = ft.tsthreefigTickLabels(fig,period=(dt_end-dt_start),period2=(dt_end2-dt_start2))
    
    # setting upp the axis stuff
    seis_axis2 = fig.axes[0]
    seis_maxes2 = [seis_axis2, seis_axis2.twinx()]
    seis_axis2.set_xlim([dt_start2,dt_end2])
  
    seis_axis = fig.axes[1] 
    seis_maxes = [seis_axis, seis_axis.twinx()]
    seis_axis.set_xlim([dt_start,dt_end])
    
    gps_axis = fig.axes[2]
    gps_maxes = [gps_axis]
    gps_axis.set_xlim([dt_start,dt_end])
    fig.subplots_adjust(right=1)
    
    #create the plot 
    plc.plotSeis(Seis2, seis_axis2, seis_maxes2, sid, constr)
    plc.plotSeis(Seis, seis_axis, seis_maxes, sid, constr) 
    plc.plotGPS(GPS, gps_axis, gps_maxes, sid, constr)
 
    # write to image file
    home = Path.home()
    relpath = "multiplot/figures/"
    filebase = "threePlot"
    filename = home.joinpath(relpath,filebase).as_posix()
    #filename = "/home/gpsops/multiplot/figures/multiplot"
    print("threePlot.png created")
    tplt.saveFig(filename, "png", fig)
    del fig
