#!/usr/bin/env python
import argparse
import choosePlot as Plot

import argparse
import sys
#lets python look in the right module...
sys.path.insert(1, '/home/sindrim/anaconda3/Verkefni/multiplots/configFolder/')
sys.path.insert(1, '/home/sindrim/anaconda3/Verkefni/multiplots/tickLabels/')
import plotLatLon as latLon
from configparser import ConfigParser

file = r'/home/sindrim/anaconda3/Verkefni/multiplots/configFolder/config.ini'
config = ConfigParser()
config.read(file)

def main():
    
    #Set q-default as 99 accuracy/validity
    q = [99]
    parser = argparse.ArgumentParser(description="Graphing earthquakes, gps coordinates program",add_help=True)
    
    #Time perod parsing, first period  and station name are mandatory
    parser.add_argument('x', type=int, nargs=1, help="Type in the starting time for the period, e.g: -100")
    parser.add_argument('y', type=int, nargs=1, help="Type in the end time of the period, e.g: 0")
    parser.add_argument('S',type=str,help="Type in the name of the gps station in CAPITAL LETTERS")
    
    #parser.add_argument('--O', action='store', type=int, nargs=2, help="Type in time period, e.g --one -100 0")
    parser.add_argument('--T', action='store', type=int, nargs=2, help="Type in second time period , e.g: --T -5 0")
    parser.add_argument('--N', action='store', type=int, nargs=1, help="Type in the number of figures to plot, valid numbers are 2,3 and 4. E.g: --N 2")
    parser.add_argument('--Q', type=int, nargs=1, help='Type in the Q variable for earthquakes, e.g: --Q 60')
    
    args = parser.parse_args()
    
    fig_num = args.N
    s = args.S
    input2 = args.T
    x = args.x
    y = args.y   
    
    # If --Q was choosen then swap out the q-default value for the new one.
    if args.Q != None:
        #q = [args.Q]
        del q 
        q = args.Q
    
    # need to find out if the selected system has gas/hytro data.
    checkGas = None
    if config.get(s,'isGas') == 'true':
        checkGas = True
    else:
        checkGas = False
    
    #Converting/Casting the input parser N from a string to a list of int if it is not None.
    #Becomes a list with 1 element, from there it's easy to use as an Integer or float.
    if fig_num != None:
        map_object = map(int, fig_num)
        lst = list(map_object)
        num = lst[0]
    
    ### Select the appropriate plots based on date input from parser, if the seismic system has 
    ### gas/hytro data in config file and/or if N has been selected
    
    if len(s) == 5: # if the input string has an extra letter(2) then plot latlon
        print(q[0])
        latLon.plotFig(x[0], y[0], input2[0], input2[1], q[0], s)
        
    elif input2 == None:
        if checkGas and fig_num == None:
            Plot.fourPlot(x[0], y[0], s)
            
        elif checkGas and num == 2:
            Plot.twoPlot(x[0], y[0], s)
            
        else:
            Plot.twoPlot(x[0], y[0], s)
            
    elif input2:
        Plot.threePlot(x[0], y[0], input2[0], input2[1], s)
    
if __name__=="__main__":
    main()

    