#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function

from pylab import *
import numpy as np
import geofunc.geo as geo
import datetime
import pandas as pd
from datetime import datetime
from datetime import timedelta
from matplotlib.colors import LinearSegmentedColormap

from matplotlib import cm
import matplotlib
matplotlib.pyplot.stem
matplotlib.axes.Axes.stem

from configparser import ConfigParser

file = r'/home/sindrim/anaconda3/Verkefni/multiplots/configFolder/config.ini'
config = ConfigParser()
config.read(file)

from datetime import datetime as dt
timestamp_format="%a %-d.%b %Y, %H:%M"
timestamp=dt.now()
dstr="%Y-%m-%d %H:%M:%S"
dfstr="%Y-%m-%d"  # date format

#######################################################
#####                                         #########
#####       Funtions for latlon plots         #########
#####                                         #########
#######################################################

####   Scatter Longitude earthquakes
def plotLon(Q2, Y2, seis_maxes, constr, seis_axis, dt_start2, pend2, sid):

    min_lon = float(config.get(sid,'min_lon'))
    max_lon = float(config.get(sid,'max_lon'))

    #Set axis limit
    seis_maxes[0].set_zorder(0) 
    seis_maxes[0].set_ylim(min_lon, max_lon)
    seis_maxes[1].set_yticks([])
    
    seis_maxes[0].set_xlim([dt_start2, pend2])
    seis_maxes[0].set_ylabel("Longitude", fontsize=20, labelpad=4)
    #seis_maxes[0].set_ylim(-19.3,-17)
    
    seis_maxes[0].scatter(Y2.index, Y2["longitude"], alpha = 0.75, c=Y2['Mcolors'], label='Cumulative moment', zorder=4, s=4*3.5**Y2['MLW'], marker = 'o',edgecolors='face')
    
    seis_maxes[0].scatter(Q2.index, Q2["longitude"], alpha = 0.75, c=Q2['Mcolors'], label='Cumulative moment', zorder=4, s=4*3.5**Q2['MLW'], marker = 'o',edgecolors='black')
    
    area = [ "min_lon",  "max_lon", "min_lat",  "max_lat"]
    seis_infotext="Seismicity in the GJOG area\n {:.2f} to {:.2f} and {:.2f} to {:.2f}".format(*[ constr[key] for key in  area ] ) 
    seis_axis.text(0.50, 0.90, seis_infotext, fontsize=15, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='center'  )

    return
    


####   Scatter Latitude earthquakes...
def plotLat(Q2, Y2, seis_maxesLat, dt_start2, pend2, sid):
    
    min_lat = float(config.get(sid,'min_lat'))
    max_lat = float(config.get(sid,'max_lat'))
   
    seis_maxesLat[0].set_zorder(0)
    seis_maxesLat[1].set_zorder(1)
    seis_maxesLat[0].set_xlim([dt_start2, pend2])
    
    #y-axis not always showing up correctly
    seis_maxesLat[0].set_ylim([min_lat, max_lat])
    seis_maxesLat[1].set_yticks([])
    seis_maxesLat[0].set_ylabel("Latitude", fontsize=20, labelpad=4)
    #Scatter Latitude earthquakes
    seis_maxesLat[0].scatter(Y2.index, Y2["latitude"], alpha = 0.75, c=Y2['Mcolors'], zorder=4, s=4*3.5**Y2['MLW'], marker = 'o',edgecolors='face')
    
    seis_maxesLat[0].scatter(Q2.index, Q2["latitude"], alpha = 0.75, c=Q2['Mcolors'], zorder=4, s=4*3.5**Q2['MLW'], marker = 'o',edgecolors='black')

    return



def plotDepth(Q2,Y2, seis_maxesDepth, dt_start2, pend2):
    
    seis_maxesDepth[0].set_xlim([dt_start2, pend2])
    seis_maxesDepth[0].set_zorder(1)
    seis_maxesDepth[0].patch.set_visible(False)

    seis_maxesDepth[1].scatter(Y2.index, Y2["depth"], alpha = 0.75, c=Y2['Mcolors'], label='Cumulative moment', zorder=4, s=4*3.5**Y2['MLW'], marker = 'o',edgecolors='face')
    
    seis_maxesDepth[1].scatter(Q2.index, Q2["depth"], alpha = 0.75, c=Q2['Mcolors'], label='Cumulative moment', zorder=4, s=4*3.5**Q2['MLW'], marker = 'o',edgecolors='black')
    
    seis_maxesDepth[0].set_ylim(20, 0)
    seis_maxesDepth[1].set_yticks([])

    return



def plotGps(GPS, gps_maxes,gps_axis, start, pend, sid):
    
    gps_maxes[0].set_zorder(1)
    gps_maxes[0].patch.set_visible(False)
    gps_maxes[0].set_xlim([start, pend])
    gps_maxes[0].errorbar(GPS.index, GPS.hlength, yerr=GPS.Dhlength, ls='none', ecolor='grey', elinewidth=0.2, 
                     capsize=0.6, capthick=0.2)
    gps_maxes[0].set_ylabel("Displacement\n[mm]", fontsize=20, labelpad=4)
    gps_maxes[0].plot_date(GPS.index, GPS.hlength, marker='o', markersize=8, markerfacecolor='r', 
               markeredgecolor='r', label='Horizontal displacemt')

    gps_maxes[0].errorbar(GPS.index, GPS.up, yerr=GPS.Dup, ls='none', ecolor='grey', elinewidth=0.2, 
                         capsize=0.6, capthick=0.2)
    #gps_maxes[1].set_ylabel("Vertical\ndisplacement [mm]", fontsize='large', labelpad=4)
    gps_maxes[0].plot_date(GPS.index, GPS.up, marker='o', markersize=8, markerfacecolor='black', 
               markeredgecolor='black', label='Vertical displacemt')
    gps_maxes[0].legend(loc=(0.05,0.67), numpoints=1, frameon=True, edgecolor="white", markerscale=2, framealpha=0.5)

    gps_infotext="GPS station {}".format(sid)
    gps_axis.text(0.5, 0.9, gps_infotext, fontsize=15, transform=gps_axis.transAxes, 
                  bbox=dict(facecolor='orange', alpha=0.1), ha='center' )
    
    return



#this plot is drawn on top of the gps plot:    plotGps(GPS,gps_maxes,gps_axis, start, pend, sid):
def plotMomentAndcolorCodeQukes(Seis, start, pend, gps_maxes):
    
    # this line needs to be improved, causes anomaly/abnormal line in the moment plot, can be pretty ridiculous 
    # with shorter time periods.
    Seis['cum_moment'].fillna(method='ffill', inplace=True)
    
    xlsc=1.02 # x-axis limits w.r.t. data
    ylsc=1.05 # y-axis limits w.r.t. data (max.abs)
    moment_scale=1e14
    ylim_magnitude=np.array([0, ylsc*max(Seis.MLW)])
    ylim_moment=np.array([0, ylsc*max(Seis.cum_moment)])

    gps_maxes[1].plot(Seis.index, Seis.cum_moment/moment_scale, color="black", zorder=4 , label= 'asdasdad')
   
    print("Seis.cum_moment/moment_scale")
    print(Seis.cum_moment)
    now = Seis.index.max()
    
    gps_maxes[1].fill_between(Seis.index, 0, Seis.cum_moment/moment_scale, facecolor=(.8,.8,.8), alpha=0.4, label = 'tsdast')
    cmap = matplotlib.colors.LinearSegmentedColormap.from_list("",["blue","orange","red"])
    
    #create a smaller dataframe for earthquakes bigger tan 4.5
    theBigOnes = Seis.loc[Seis['MLW'] >= 4.5]

    texts = ["Moment magnitude [$\mathrm{M_{W}}$] for reviewed events) / Local magnitude [$\mathrm{M_{L}}$]  for not reviewed events"]
    
    patches = [ plt.plot([],[], marker="o", ms=15,  mec='black', color='gray', 
            label="{:s}".format(texts[i]))[0]  for i in range(len(texts)) ]
   
    gps_maxes[2].legend(facecolor=None, framealpha=0, handles=patches,bbox_to_anchor=(0.215, 0.90), 
           loc='center', ncol=2, numpoints=1 ,fontsize = 20)
    
    gps_maxes[2].patch.set_visible(False)
    gps_maxes[2].set_zorder(5)
    gps_maxes[2].set_xlim([start, pend])
    
    #Plot colorcoded earthquakes by date
    gps_maxes[2].scatter(Seis.index, Seis.MLW, c=Seis.index, cmap=cmap, s = 100,  marker =  "o", linewidth = 0.3, edgecolors='k')
    
    #   Bigger earthquakes are drawn twice to
    #   the lime green stars, inefficient, but they are not many.
    gps_maxes[2].scatter(theBigOnes.index, theBigOnes.MLW, c= 'lime', cmap=cmap, s = 650,  marker =  "*", linewidth = 0.4, edgecolors='k')
    
    gps_maxes[-1].spines['right'].set_position(('axes', 1.05))
    gps_maxes[2].spines['right'].set_position(('axes', 1.05))
    
    gps_maxes[1].set_zorder(3)
    gps_maxes[1].patch.set_visible(False)
    gps_maxes[1].set_xlim([start, pend])
    gps_maxes[1].set_ylabel("Seismic moment\n [$10^{14}\,$Nm]", fontsize=24, labelpad=4)
    
    gps_maxes[3].set_zorder(1)
    gps_maxes[3].set_xlim([start, pend])
    gps_maxes[3].stem(Seis.index,  Seis.MLW, 'gray', markerfmt=" ", use_line_collection = True)
    gps_maxes[3].set_ylabel("Magnitude [$\mathrm{M_{W}}$/$\mathrm{M_{L}}$]", fontsize=24, labelpad=4,)
    
    gps_maxes[0].set_ylim(ymin=-30, ymax = 30)
    gps_maxes[1].set_ylim(*ylim_moment/moment_scale*1.5)
    gps_maxes[2].set_ylim(ymin = 0, ymax = 11)
    gps_maxes[3].set_ylim(ymin = 0, ymax = 11)
    
    return


"""
Display the timestamp (green/red colorwarn) on the graph
"""
def annotateTimeUpdate(seis_maxesDepth,gps_maxes,dt_end, dt_end2,color_warn,color_warn2):

    # round the timestamp to lst second
    df = pd.DataFrame({'timestamp':[timestamp]})
    newTimestamp = df.timestamp.dt.round('1s')
    newTimestamp = newTimestamp.item() # .item() does something to allow .annotate to compile the new timestamp 

    seis_maxesDepth[1].annotate(newTimestamp, xy=(dt_end2+timedelta(0), 0), xytext=(10,250), textcoords='offset points', horizontalalignment='left', fontsize=20, rotation="vertical", color=color_warn2)
    
    seis_maxesDepth[1].annotate(newTimestamp, xy=(dt_end2+timedelta(0), 0), xytext=(10,700), textcoords='offset points', horizontalalignment='left', fontsize=20, rotation="vertical", color=color_warn2)
    
    seis_maxesDepth[0].set_ylabel("Depth [km]", fontsize=20, labelpad=4)

    gps_maxes[1].annotate(newTimestamp, xy=(dt_end+timedelta(0), 0), xytext=(10,50), textcoords='offset points', horizontalalignment='left', fontsize=20, rotation="vertical", color=color_warn)
    
    return
    
    
"""
Old and probably useless
"""
def setLandmarks(seis_axis, seis_axisLat, sid, Seis):
    landmarks = getLandmarks(sid)
    if landmarks != None:            #Testing
        print(landmarks[3])
        for i in range(0, len(landmarks)):
            print(landmarks[i])      #Testing
        """
        #Landmarks.. Will be stored with configparser later on as text
        seis_axis.text(0.01, 0.85, "Husavik", fontsize= 23, transform=seis_axis.transAxes,
                           bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
        seis_axis.text(0.01, 0.420, "Gjogurta", fontsize= 23, transform=seis_axis.transAxes,
                           bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
        seis_axis.text(0.01, 0.16, "Eyjafjardarall", fontsize= 23, transform=seis_axis.transAxes,
                           bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
        seis_axis.text(0.01, 0.62, "Flatey", fontsize= 23, transform=seis_axis.transAxes,
                           bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
        seis_axisLat.text(0.01, -0.98, "Husavik", fontsize= 23, transform=seis_axis.transAxes,
                           bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
        seis_axisLat.text(0.0051, -0.8, "Gjogurta", fontsize= 23, transform=seis_axis.transAxes,
                           bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
        seis_axisLat.text(0.01, -0.22, "Eyjafjardarall", fontsize= 23, transform=seis_axis.transAxes,
                           bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
        """
    return



"""
set Horizontal Lines if they are described in thee config.ini file
reads them from config.ini and displays them.
"""
def setHorizLinesWithLandmLabels(seis_maxes, seis_maxesLat, Q2, dt_end2, dt_start2, sid):
    
    #colors = ['green','blue','gold','red'] # could use this line to color code the lines along with landmarks
    landmarksLon = gethLonLandmarks(sid)
    landmarksLat = gethLatLandmarks(sid)
    
    hLonLines = gethLonLines(sid)
    hLatLines = gethLatLines(sid)
    
    #Plot Lonlines if they exist
    if hLonLines != None:
        #Set Lon lines
        for i in range(0,len(hLonLines)):
            str1 = str(hLonLines[i])
            seis_maxes[0].hlines(y= hLonLines[i], xmin=dt_start2, xmax=dt_end2, color = 'gray', zorder=1,
                                    label =landmarksLon[i] + " " + str1, linewidth=1.5)
        
    #Plot Latlines if they exist
    if hLatLines != None:
        #Set Lat lines
        for i in range(0, len(hLatLines)):
            str2 =  str(hLatLines[i])
            seis_maxesLat[0].hlines(y= hLatLines[i], xmin=dt_start2, xmax=dt_end2, color= 'gray', zorder=1,
                                    label = landmarksLat[i] + " " + str2, linewidth=1.5)
    
    if hLonLines != None:
        #store legend to plot later with earthquake size legend.  
        leg1 = seis_maxes[0].legend(loc = 'upper left') # this is so we can put two legend boxes on the graph
        setpatch(seis_maxes)        # patch legend 
        seis_maxes[0].add_artist(leg1)  # Landmark legend
    else:
        setpatch(seis_maxes)
    
    if hLatLines != None:
        seis_maxesLat[0].legend(loc = 'upper left')
        
    return
    
    

"""
Set the colors for earthquake sizes
"""
def setSeisColors(Seis):
    Seis['Mcolors'] = float('nan')
    Seis.loc[  Seis['MLW'] < 2 , 'Mcolors'] = ['green']
    Seis.loc[ ( Seis['MLW'] >= 2 ) & ( Seis['MLW'] < 3 ) , 'Mcolors'] = 'gold'
    Seis.loc[ ( Seis['MLW'] >= 3 ) & ( Seis['MLW'] < 4 ) , 'Mcolors'] = 'darkorange'
    Seis.loc[ ( Seis['MLW'] >= 4 ) & ( Seis['MLW'] < 5 ) , 'Mcolors'] = 'orangered'
    Seis.loc[ ( Seis['MLW'] >= 5 ) & ( Seis['MLW'] < 6 ) , 'Mcolors'] = 'red'
    Seis.loc[ ( Seis['MLW'] >= 6 ), 'Mcolors'] = 'darkred'
    return  Seis['Mcolors'] 
    
    

"""
Lines right before color warning, to seperate the current time and updated earthqukes 
"""
def setVerticalUpdateLines(seis_maxes, seis_maxesLat, seis_maxesDepth , gps_maxes, dt_end, dt_end2 ):
    #Time update vertical lines
    seis_maxes[0].axvline(dt_end2, color='gray')
    seis_maxesLat[0].axvline(dt_end2, color='gray')
    seis_maxesDepth[0].axvline(dt_end2, color='gray')
    gps_maxes[0].axvline(dt_end, color='gray')
    
    return


"""
Create a title for the graph, e.g:  "Graf gert: Wed 15.Jul 2020, 11:50" 
"""
def getColorWarnTitle():
    Title = "Graf gert: {}".format( timestamp.strftime(timestamp_format) )
    return Title



"""
check if the updates are on time or are at the current time
"""
def setColorWarn(dt_end):
    if (timestamp - dt_end) > timedelta(minutes=15):
        color_warn="r"
    else:
        color_warn="g"
    
    return color_warn
    
    
"""
display color coded earthqukes for different sizes on the top graph
"""
def setpatch(seis_maxes):
    #markSize = [22.5, 20, 17.5, 15, 12.5, 10] 
    patchColors = ["darkred", "red","orangered","darkorange","gold","green"]
    texts = ["6+", "5-6", "4-5", "3-4", "2-3", "0-2"]
    
    patches = [ plt.plot([],[], marker="o", ms=25,  mec='black', color=patchColors[i], 
            label="{:s}".format(texts[i]))[0]  for i in range(len(texts)) ]
    
    seis_maxes[0].legend(facecolor=None, framealpha=0, handles=patches,bbox_to_anchor=(0.15, 0.87), 
           loc='center', ncol=2, numpoints=1 ,fontsize = 20)
    return



"""
get all landmarks, weather it's lon or lat
"""
def getLandmarks(sid):
    check = config.has_option(sid,'landmarks') #check if landmarks exist
    if check:
        landmarks = config.get(sid,'landmarks')
        cut = len(landmarks)-1
        cut = slice(1,cut)
        landmarks = [idx for idx in landmarks[cut].split(',')]
        
        return landmarks
    else:
        return 

    
""" 
#Get landmarks for Longitude    
"""
def gethLonLandmarks(sid):
    check = config.has_option(sid,'landmarksLon') #check if landmarks exist
    if check:
        landmarks = config.get(sid,'landmarksLon')
        cut = len(landmarks)-1
        cut = slice(1,cut)
        landmarks = [idx for idx in landmarks[cut].split(',')]
        
        return landmarks
    else:
        return 

    
"""
#Get landmarks for Latitude
"""
def gethLatLandmarks(sid):
    check = config.has_option(sid,'landmarksLat') #check if landmarks exist
    if check:
        landmarks = config.get(sid,'landmarksLat')
        cut = len(landmarks)-1
        cut = slice(1,cut)
        landmarks = [idx for idx in landmarks[cut].split(',')]
        
        return landmarks
    else:
        return 

    
"""
#Get Longitude coordinates 
"""
def gethLonLines(sid):
    check = config.has_option(sid,'hLonLines') #check if landmarks exist
    if check:
        hLonLines = config.get(sid,'hLonLines')
        cut = len(hLonLines)-1
        cut = slice(1,cut)
        hLonLines = [float(idx) for idx in hLonLines[cut].split(',')]
        return hLonLines
    else:
        return

    
"""   
#Get Latitude coordinates
"""
def gethLatLines(sid):
    check = config.has_option(sid,'hLatLines') #check if landmarks exist
    if check:
        hLatLines = config.get(sid,'hLatLines')
        cut = len(hLatLines)-1
        cut = slice(1,cut)
        hLatLines = [float(idx) for idx in hLatLines[cut].split(',')]
        
        return hLatLines
    else:
        return 

    

def getConstr(sid):
    min_lat = float(config.get(sid,'min_lat'))
    max_lat = float(config.get(sid,'max_lat'))
    min_lon = float(config.get(sid,'min_lon'))
    max_lon = float(config.get(sid,'max_lon'))
    constr = {
                    "min_lat" : min_lat,  "max_lat":  max_lat,
                    "min_lon":  min_lon, "max_lon":  max_lon,
    }
    return constr


def getLoc():
    loc = "IMO"
    if loc=="IMO":
        conn_dict={ 
               "dbname"    : "gas",
               "user"      : "gas_read",
               "password"  : "qwerty7",
               "host"      : "dev.psql.vedur.is",
               "port"      :  5432
               }
    elif loc=="home":
        # runn to make this work
        #ssh -f gpsops@rek.vedur.is -L 5431:dev.psql.vedur.is:5432 -N
        conn_dict={
                "dbname"    : "gas",
                "user"      : "gas_read",
                "password"  : "qwerty7",
                "host"      : "localhost",
                "port"      :  5431
                }
    return conn_dict


"""
set pend as the ending time for displaying the data on the graph 
"""
def getPend(num, dt_end, dt_start):
    if num == 0:
        pend = timestamp + (dt_end - dt_start)/40
    else:
        pend = dt_end + (dt_end - dt_start)/40
    
    return pend
        


"""
Get the data from database for seismic activity
"""
def getDataBase(dt_start, dt_end, sid):
    import geo_dataread.sil_read as gdrsil
    pre_path= "/mnt_data/"
    sil_path="sildata/"
    col_names = ['latitude', 'Dlatitude', 'longitude', 'Dlongitude', 
                    'depth', 'Ddepth', 'MLW', 'seismic_moment', 'cum_moment', 'Q']
    
    constr = getConstr(sid)
    Seis = gdrsil.read_sil_data(start=dt_start, end=dt_end, base_path=pre_path+sil_path,  
            fread="default", aut=True, aut_per=2, rcolumns=col_names, cum_moment=True, logger='default', **constr)
    
    return Seis


"""
Get the gps data from database for gps coordinate displacements
"""
def getGPS(sid, dt_end, dt_start):
    from gtimes.timefunc import TimetoYearf
    import geo_dataread.gps_read as gdrgps
    pre_path= "/mnt_data/"
    logging_level=logging.INFO
    ## Get GPS data with some basic filtering
    gps_path="gpsdata/"
    #-#detrend_period=[TimetoYearf(2001,1,1), TimetoYearf(2010,1,1)] # FOR HVOL
    #-#detrend_line=[TimetoYearf(2010,6,1), TimetoYearf(2014,4,1)] # FOR HVOL
    ## Get GPS data with some basic filtering
    detrend_period=[TimetoYearf(2001,1,1), TimetoYearf(2019,1,1)]
    detrend_line=[TimetoYearf(2010,6,1), TimetoYearf(2019,4,1)]
    GPS = gdrgps.read_gps_data(sid, Dir=pre_path+gps_path, start=dt_start, end=dt_end, detrend_period=None, 
                   detrend_line=None, logging_level=logging_level)

    return GPS


"""
get the second time period ending date
"""
def getEndDate(timeRange2):
    from datetime import datetime as dt
    import gtimes.timefunc as tf
    
    dstr="%Y-%m-%d %H:%M:%S"
    dfstr="%Y-%m-%d"  # date format
    
    now = datetime.now()
    
    if timeRange2 == 0:
        end_date2=dt.strftime(dt.now(), dstr)
    else:
        end_date2 = tf.currDatetime(timeRange2)
    
    return end_date2


"""
simple function for testing config
"""
def getLandmarknames(sid):  
    str1 = config.get(sid,'landmarks')
    print(str1)
    return 



"""
Created while configuring the modules
"""
def testfunction():
    print("This function is only here for module functionality checking")
    return




    
########################################################################
######                                                          ########
######     Below this are functions for two-three-fourplots     ########
######                                                          ########
########################################################################
def plotSeis(Seis, seis_axis, seis_maxes, sid, constr):
    
    xlsc=1.02 # x-axis limits w.r.t. data
    ylsc=1.05 # y-axis limits w.r.t. data (max.abs)
    ## ploting seismic
    moment_scale=1e14
    ylim_magnitude=np.array([0, ylsc*max(Seis.MLW)])
    ylim_moment=np.array([0, ylsc*max(Seis.cum_moment)])
    
    ###    laga cum_moment ####
    Seis['cum_moment'].fillna(method='ffill', inplace=True)
    #Seis['cum_moment'].fillna(method='ffill')
    
    
    # moment plot
    seis_maxes[0].set_zorder(1)
    seis_maxes[0].set_ylabel("Seismic moment\n [$10^{14}\,$Nm]", fontsize='large')
    seis_maxes[0].plot(Seis.index, Seis.cum_moment/moment_scale, color="black", label='Cumulative moment', zorder=4)
    
    seis_maxes[0].fill_between(Seis.index, 0, Seis.cum_moment/moment_scale, facecolor=(.8,.8,.8), alpha=0.4)
    
    seis_maxes[0].set_ylim(*ylim_moment/moment_scale)
    
    seis_maxes[0].legend(loc=(0.05,0.79), numpoints=3, frameon=True, edgecolor="white", markerscale=1, framealpha=0.5)
    # stem plot
    seis_maxes[1].set_zorder(5)
    seis_maxes[1].set_ylabel('Magnitude', fontsize='large')
    
    markerline, stemlines, baseline = seis_maxes[1].stem(Seis.index, Seis.MLW,
                use_line_collection=True)
    plt.setp(stemlines, ls="-", color=(.6,.6,.6), lw=0.4, alpha=1.0)
    plt.setp(markerline, mfc='blue', mec='blue', ms=6, label='Magnitude $\mathrm{M_{LW}}$')
    plt.setp(baseline, visible=False, alpha=0.1)
    #seis_maxes[0].set_xlim(xlim[0],xlim[1])
    seis_maxes[1].set_ylim(*ylim_magnitude)
    seis_maxes[1].legend(loc=(0.04,0.66), numpoints=1, frameon=True, edgecolor="white", markerscale=1, framealpha=0.5)
    area = [ "min_lon",  "max_lon", "min_lat",  "max_lat"]

    seis_infotext= sid + "Seismicity in the the area \n {:.2f} to {:.2f} and {:.2f} to {:.2f}".format(*[ constr[key] for key in  area ] ) 
    seis_axis.text(0.50, 0.80, seis_infotext, fontsize=15, transform=seis_axis.transAxes,
                   bbox=dict(facecolor='orange', alpha=0.1), ha='center'  )
    return



"""
"""
def plotGPS(GPS, gps_axis, gps_maxes, sid, constr):
    ## ploting GPS
    xlsc=1.02 # x-axis limits w.r.t. data
    ylsc=1.05 # y-axis limits w.r.t. data (max.abs)
    ylim_hlength=np.array([ylsc*min(GPS.hlength), ylsc*max(GPS.hlength)])
    
    gps_maxes[0].errorbar(GPS.index, GPS.hlength, yerr=GPS.Dhlength, ls='none', ecolor='grey', elinewidth=0.2, 
                     capsize=0.6, capthick=0.2)
    gps_maxes[0].set_ylabel("Displacement\n[mm]", fontsize='large', labelpad=4)
    gps_maxes[0].plot_date(GPS.index, GPS.hlength, marker='o', markersize=5, markerfacecolor='r', 
               markeredgecolor='r', label='Horizontal displacemt')

    gps_maxes[0].errorbar(GPS.index, GPS.up, yerr=GPS.Dup, ls='none', ecolor='grey', elinewidth=0.2, 
                         capsize=0.6, capthick=0.2)
    #gps_maxes[1].set_ylabel("Vertical\ndisplacement [mm]", fontsize='large', labelpad=4)
    gps_maxes[0].plot_date(GPS.index, GPS.up, marker='o', markersize=5, markerfacecolor='b', 
               markeredgecolor='b', label='Vertical displacemt')
    gps_maxes[0].legend(loc=(0.05,0.67), numpoints=1, frameon=True, edgecolor="white", markerscale=1, framealpha=0.5)

    gps_infotext="GPS station {}".format(sid)
    gps_axis.text(0.5, 0.9, gps_infotext, fontsize=15, transform=gps_axis.transAxes, 
                  bbox=dict(facecolor='orange', alpha=0.1), ha='center' )

    return




"""
"""
def plotHytro(Hytro,hytro_axis,hytro_maxes,hytro_station):

    TW1_color=(.6,.6,.6)
    TL1_color=(.8,.8,.8)
    hytro_maxes[2].set_zorder(1)
    hytro_maxes[2].patch.set_visible(False)
    hytro_maxes[2].set_ylabel("Temperature [$^{\circ}$C]", fontsize='large', labelpad=4)
    TW_plot = hytro_maxes[2].plot_date(Hytro.index, Hytro.Tw1_med_C, marker='o', markersize=1, markerfacecolor=TW1_color, 
               markeredgecolor=TW1_color, label="Water Temperature")
    TL_plot = hytro_maxes[2].plot_date(Hytro.index, Hytro.TL1_med_C, marker='o', markersize=1, markerfacecolor=TL1_color, 
               markeredgecolor=TL1_color, label="Air Temperature")

    hytro_maxes[0].set_zorder(10)
    hytro_maxes[0].patch.set_visible(False)
    hytro_maxes[0].set_ylabel("Conductivity\n[$\mu\mathrm{S\,cm^{-1}}$]", fontsize='large', labelpad=4)
    EC_plot = hytro_maxes[0].plot_date(Hytro.index, Hytro.EC_med_us, marker='o', markersize=1, markerfacecolor='r', 
               markeredgecolor='r', label="Conductivity",  alpha=1.0)
    hytro_maxes[0].set_ylim(ymin=50) #CO2_max])

    hytro_maxes[1].set_zorder(3)
    hytro_maxes[1].patch.set_visible(False)
    hytro_maxes[1].set_ylabel("Water level [m]", fontsize='large', labelpad=4)
    Wl_plot=hytro_maxes[1].plot_date(Hytro.index, Hytro.W1_med_cm/100, marker='o', markersize=1, markerfacecolor='b', 
               markeredgecolor='b', label="Water level", alpha=1.0)

#hytro_axis.legend( (EC_plot), loc=(0.20,0.67), ncol=4, numpoints=1, frameon=True, edgecolor="white", markerscale=5, framealpha=0.5)
    hytro_maxes[2].legend(loc=(0.15,0.67), numpoints=1, frameon=True, edgecolor="white", markerscale=5, framealpha=0.5)
    hytro_maxes[0].legend(loc=(0.00,0.80), numpoints=1, frameon=True, edgecolor="white", markerscale=5, framealpha=0.5)
    hytro_maxes[1].legend(loc=(0.00,0.67), numpoints=1, frameon=True, edgecolor="white", markerscale=5, framealpha=0.5)

    hytro_infotext="Hytrological station in Mulakvísl {}".format(hytro_station)
    hytro_axis.text(0.50, 0.9, hytro_infotext, fontsize=15, transform=hytro_axis.transAxes,
                   bbox=dict(facecolor='orange', alpha=0.1), ha='center' )

    return


 
"""
"""
def plotGas(GAS, gas_axis, gas_maxes, station):
   
    gas_maxes[0].set_zorder(1)
    #CO2_max=GAS.co2_percentage1]*100+3
    gas_maxes[0].set_ylabel("Consentration [$\%$]", fontsize='large', labelpad=4)
    gas_infotext= station
    try:
        gas_maxes[0].plot_date(GAS.index, GAS.co2_percentage, marker='o', markersize=5, markerfacecolor='black', 
                markeredgecolor='black', label="CO$_2$ [$\%$]", zorder=1)

        gas_maxes[1].set_zorder(1)
        maxgas = max(max(GAS["h2s_concentration"]) , max(GAS["so2_concentration"] ))
        #gas_maxes[1].set_ylim([-0.1,2.5*maxgas])
        gas_maxes[1].set_ylabel("Consentration\n[ppm]", fontsize='large', labelpad=4)
        gas_maxes[1].plot_date(GAS.index, GAS.h2s_concentration, marker='o', markersize=5, markerfacecolor='red', 
                   markeredgecolor='red', label='H$_2$S [ppm]',zorder=1)

        gas_maxes[1].plot_date(GAS.index, GAS.so2_concentration, marker='o', markersize=5, markerfacecolor='b', 
                  markeredgecolor='b', label='SO$_2$ [ppm]')
        gas_maxes[1].set_ylim(ymin=0, ymax=1.05*maxgas)
        #legend
        gas_maxes[1].legend(loc=(0.05,0.66), numpoints=1, frameon=True, edgecolor="white", framealpha=0.1)
        gas_maxes[0].legend(loc=(0.05,0.52), numpoints=1, frameon=True, edgecolor="white", framealpha=0.1)

        gas_axis.text(0.50, 0.9, gas_infotext, fontsize=15, transform=gas_axis.transAxes,
                      bbox=dict(facecolor='orange', alpha=0.1), ha='center' )
    except AttributeError:
        gas_axis.text(0.5, 0.5, gas_infotext+"\nNo Gas data found", fontsize=35, transform=gas_axis.transAxes,
                      bbox=dict(facecolor='orange', alpha=0.1), ha='center' )

    gas_maxes[0].set_ylim(ymin=0) #CO2_max])
    
    return


 
"""
"""
def getHytroData(hytro_station, dt_start, dt_end):
    import geo_dataread.hytro_read as gdrhyt
    
    pre_path= "/mnt_data/"
    logging_level=logging.INFO
    #station="lagu"
    ## Get hytrological data
    hytro_path="hytrodata/" 
    Hytro = gdrhyt.read_hytrological_data(hytro_station,start=dt_start, end=dt_end, frfile=False, fname="hytro-data.pik", 
                                 wrfile=False, base_path=pre_path+hytro_path, logging_level=logging_level )
    return Hytro



"""
"""
def getGasData(station, dt_start, dt_end):
    import geo_dataread.gas_read as gdrgas
    
    conn_dict = getLoc()
    device="crowcon"
    logging_level=logging.INFO
    fname = "{0:s}_{1:s}.p.gz".format(station,device)
    dsource="DBASE"
    GAS = gdrgas.read_gas_data(station, device, start=dt_start, end=dt_end, 
                             dsource=dsource, wrfile=False, fname=fname, with_excluded=True, 
                             id_observation_start=False, conn_dict=conn_dict, logging_level=logging_level)
    return GAS








    