# ProcessingGpsData

Automatic processing of continuous gps data

This code is work in process.


## Running from the command line


Running: <br/>
```
./getplot2 x y stationName
```
<br/>
x, y, stationName are mandatory inputs<br/>

--N choses the number of Images/graphs on the plot<br/>

--T allows for two time periods <br/>

--Q sets the validity value of th earthquakes<br/>
<br/>
<br/>

--N tells the program to create only two figures<br/>
```
./getPlot2.py  -100 0 AUST  --N 2 
```
<img src = "Images/twoPlot.png" width = "900">
<br/>
<br/>
<br/>
<br/>
<br/>
--T tells the program to create two time periods<br/>

```
./getPlot2  -20 0 GJOG --T -4 0  
```

<br/>
<img src = "Images/threePlot.png" width = "900">

<br/>
<br/>
<br/>
<br/> 

The original multiplot<br/> 

```
./getPlot2  -100 0 AUST 
```
<br/>
<img src = "Images/multiplot.png" width = "900">

<br/>
<br/>
<br/>
<br/>


Adding 2 at the end of the input string will tell the program<br/>
to look for the station in the config file where lat long and landmarks are stored.<br/>
If they have not been specified then they will simply not be plotted.<br/>

--Q sets the value of validity of the earthquakes<br/>
Higher --Q value = more earthquakes are specified as unvarified(circles with no border).<br/>
Lover --Q value = more earthquakes specified as varified(circles with border).<br/>

```
./getPlot2  -100 0 GJOG2  --T -4 0 --Q 60 
```
<br/>
<img src = "Images/GjogPlot4.png" width = "900">

<br/>
<br/>
<br/>
<br/>

```
./getPlot2  -200 0 AUST2  --T -21 0 --Q 60 
```
<br/>
<img src = "Images/GjogPlot5.png" width = "900">




## Need to change sys paths in files for imports:
<br/>
plotLatLon.py<br/>
choosePlot.py<br/>
getPlot2.py<br/>
getPlot2.py<br/>
fourPlot_beta.py<br/>
threePlot_Beta.py<br/>
twoPlot_Beta.py<br/>
<br/>




